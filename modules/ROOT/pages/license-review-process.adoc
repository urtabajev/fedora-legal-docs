////

Copyright Fedora Project Authors.

SPDX-License-Identifier: CC-BY-SA-4.0

////

= License Review Process

This page describes how to request the review of a new license for inclusion in Fedora Linux and other related processes.

Licenses are determined to be allowed or not-allowed for inclusion in Fedora Linux as described in xref:license-approval.adoc[License Approval].

== Request review of a new license

If you find a license in a new package being submitted to Fedora, or in an existing Fedora package,
and that license is not listed on the 
xref:allowed-licenses.adoc[allowed] or xref:not-allowed-licenses.adoc[not-allowed] license lists, please follow the process described below to submit the license for review. 

You must be a Fedora contributor and become part of the Fedora Gitlab group.

. Create a new issue in the https://gitlab.com/fedora/legal/fedora-license-data/-/issues/new[Fedora License Data] using the license-review issue template.
+
NOTE: Please create one issue per license. Do not submit an issue referencing multiple licenses for review.
+
.. The license-review issue template will prompt you to include the following information:

* License name (required) -- use "None" if there is no known license name
* Text of license (required) -- provide a link to the license text, or you can paste the license text into a Gitlab issue comment 
* Fedora package name and link to upstream source code of the package (required) 
* Is the license on the https://spdx.org/licenses[SPDX License List]? (required) -- if yes, then include the applicable SPDX license expression (see below for helpful information for determining whether a license is
already on the SPDX License List)
* Reason for the license review (optional) -- for example, a link to the Bugzilla ticket for the package review
+
. All discussion related to the license review based on the Fedora xref:license-approval.adoc[license approval criteria] will be on the issue thread and a decision will be noted there. Responsibility for license approval decisions rests with the Fedora Council which normally delegates the decision to members of the Red Hat legal team.
+
.. If the license is not on the SPDX License List (or you are not sure), then https://tools.spdx.org/app/submit_new_license/[submit] the license to the SPDX-legal team. In addition to the required information, include a note that it is under review for Fedora and a link to the related Fedora License Data issue. If the license is ultimately determined to be not-allowed for Fedora, you should offer to withdraw the SPDX-legal request if it is still pending. 
+
. Once a decision has been made, create a merge request to add the license using the https://gitlab.com/fedora/legal/fedora-license-data/-/blob/master/TEMPLATE.toml[TOML file format] according the determined status (allowed, not-allowed, allowed-fonts, allowed-documentation, allowed-content, allowed-firmware).

. Merge requests will be reviewed and merged by the Fedora License Data maintainers.

=== What is a "license" for purposes of the review process?

For now, we are interpreting "license" fairly broadly. A license could be, for example:

* A complete license text

* An informal statement about how something is licensed

* A reference to an applicable license or permission terms published externally to the upstream project

* A license notice in a source file

* Minimalist grants of relatively broad legal permission (including public domain dedications)

As we gain experience with administering the new process, we may refine this view of what a "license" is in the interests of efficiency and practicality. 

=== How to determine if a license or exception is on the SPDX License List

The SPDX License List has an established set of https://spdx.github.io/spdx-spec/v2.3/license-matching-guidelines-and-templates/[guidelines] that define what constitutes a match to a https://spdx.org/licenses[license] or https://spdx.org/licenses/exceptions-index.html[exception] on the SPDX License List. This is intended to ensure that SPDX license identifiers and expressions mean the same thing wherever they are used.

Many licenses that look similar to the naked eye may have small yet substantive differences. There are some open source tools that can help detect and match licenses to those on the SPDX License List listed below. If you know of other helpful tools, please create an issue to add it to these pages!

* https://github.com/nexB/scancode-toolkit[ScanCode] - ScanCode is a comprehensive tool, written in Python, for detecting licenses and related information in source code. ScanCode output reports detected license information using both ScanCode's own non-SPDX system of license keys and corresponding SPDX expressions; however it is not clear that ScanCode uses the SPDX matching guidelines in making such determinations. 

* https://github.com/spdx/spdx-license-diff[SPDX-license-diff] - This Firefox and Chromium/Chrome plugin may be useful if there is a web interface to the upstream source repository, or if it is convenient for you to paste license text from your package into a web page. You can highlight the text of a license as displayed on a web page and it will look for the closest match to the SPDX License List, implementing the matching guidelines. If a match to an SPDX identifier is given as close to but less than 100%, SPDX-license-diff will show you differences in the texts. In some cases, differences will display that are actually accounted for by matching guidelines. If it's a close match, with the exception of small difference, such as the name of the copyright holder or author, it is recommended to cross-check against the https://github.com/spdx/license-list-XML/tree/main/src[actual template] that the SPDX license list uses to implement (some of) the matching guideliens. If a license or exception is identified as a match, you can then use the SPDX identifier to search in the *allowed* and *not-allowed* license lists for Fedora. If a license comes up as a close match to an SPDX identifier, and it does not match in the sense meant in the SPDX matching guidelines, note this in the Fedora-license-data issue and consider consulting with https://spdx.dev/participate/legal/[SPDX-legal] to see whether the license should be submitted as a new license for inclusion in the SPDX License List.

* https://tools.spdx.org/app/check_license/[SPDX Check License] (https://github.com/spdx/spdx-online-tools[source code])- This is a Django application in which you paste the text of a license or exception into a text box and it will match it against all the licenses and exceptions in the SPDX License List, implementing the SPDX Matching Guidelines. Because of its thoroughness, this tool may take more time to give an answer than SPDX-license-diff. It will tell you if there is a match or not and if it finds a close match, but won't indicate a diff.

=== What if a license is allowed for Fedora but is not on the SPDX License List?

To help Fedora switch from using "Callaway" license tags to SPDX license expressions, members of SPDX-legal undertook a comparison of all Fedora "good" and "bad" license texts in order to map Callaway licenses to SPDX license expressions. This data was later converted into the Fedora License Data TOML file format. 

However, a full SPDX mapping of all Callaway licenses was not possible. It is important to understand that the Callaway system had no counterpart to the SPDX matching concept. Some Callaway licenses are umbrella categories referring to a set of substantively different license texts (e.g., "BSD", "MIT", "exceptions"). In some of these cases, none of the license texts falling under the category were captured. In other cases, it wasn't clear that some of the older "good" licenses were even still used in Fedora Linux, in which case it did not make sense to add them to the SPDX License List. We have decided to leave these cases out of the Fedora License Data for the time being. If they are encountered in Fedora packages later, then they can be added (to the Fedora License Data and SPDX License List, as appropriate) at that point.

=== Not-allowed licenses

Any license determined to be not-allowed for Fedora will be recorded in the Fedora License Data repository using the SPDX `LicenseRef-` format, unless an SPDX license identifier is already available for that license. The license text for the not-allowed license must be captured in the TOML file, in accordance with the https://gitlab.com/fedora/legal/fedora-license-data/-/blob/master/TEMPLATE.toml[TOML template] format. As noted above, if a license has been determined to be *not-allowed*, and you have already submitted the license to the SPDX-legal team, you should offer to withdraw the submission. 

=== What is `LicenseRef-`?

The https://spdx.github.io/spdx-spec/v2.3/other-licensing-information-detected/[SPDX Specification] defines a way to create a license identifier for licenses that are not on the SPDX License List using the `LicenseRef-` prefix. For continuity and ease of identification in the data, Fedora uses the LicenseRef- convention where necessary.

Most of the licenses in the Fedora license lists that use a Fedora-defined `LicenseRef-` identifier are in the not-allowed list. Some allowed licenses that are apparently no longer present in any Fedora package may also use `LicenseRef-`.

If a license given a `LicenseRef-` identifier is later identified as a match to a license on the SPDX License List or added to the SPDX License List, then the https://gitlab.com/fedora/legal/fedora-license-data[Fedora License Data] repository should be updated.

== What if the license for a Fedora package changes?

Sometimes the upstream package you are maintaining will change the license of all or part of the project. In this case, the following guidance apply:

* If the license changes from an allowed license to a license that hasn't yet been reviewed for inclusion in Fedora, then submit the new license for review following the process described above.

* If the license changes from an allowed license to a not-allowed license, then the package can no longer be included in Fedora Linux, unless you remove the material covered by the not-allowed license. https://docs.fedoraproject.org/en-US/fesco/[FESCo] has the authority to  block upgrades of packages to versions with new licenses that are not allowed for Fedora. 

* Assuming the new license is allowed, you will have to update the spec file `License:` field (unless the License: field guidelines indicate otherwise --- for example, if the upstream license change only covers code that is not included in a Fedora binary RPM).

* Fedora package maintainers are expected to announce upstream license changes that they become aware of on the https://lists.fedoraproject.org/archives/list/devel@lists.fedoraproject.org/[Fedora devel list]. 

If you are unsure about the status of a new license in a package, please ask on the https://lists.fedoraproject.org/archives/list/legal@lists.fedoraproject.org/[Fedora legal list].








